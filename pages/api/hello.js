// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
var player = videojs('my-player');
var options = {};

var player = videojs('my-player', options, function onPlayerReady() {
  videojs.log('Your player is ready!');

  // In this context, `this` is the player that was created by Video.js.
  this.play();

  // How about an event listener?
  this.on('ended', function() {
    videojs.log('Awww...over so soon?!');
  });
});

export default function handler(req, res) {
  res.status(200).json({ name: 'John Doe' })
}
