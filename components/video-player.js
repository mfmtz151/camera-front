import { useEffect, useRef } from 'react'
import Hls from 'hls.js'

var vrView;
var playButton;
var muteButton;

export default function VideoPlayer({ src }) {
  const videoRef = useRef(null)

  useEffect(() => {
    const video = videoRef.current
    if (!video) return

    video.controls = true
    if (video.canPlayType('application/vnd.apple.mpegurl')) {
      // This will run in safari, where HLS is supported natively
      const hls = new Hls()
      hls.loadSource(src)
      hls.attachMedia(video)
      console.log("Hola1")
    } else if (Hls.isSupported()) {
      // This will run in all other modern browsers
      const hls = new Hls()
      hls.loadSource(src)
      hls.attachMedia(video)
      console.log("Hola1")

    } else {
        console.log("Hola3")

      console.error(
        'This is an old browser that does not support MSE https://developer.mozilla.org/en-US/docs/Web/API/Media_Source_Extensions_API'
      )
    }
  }, [src, videoRef])

  return (
    <>
      <video ref={videoRef} />
      <style jsx>{`
        video {
          max-width: 100%;
        }
      `}</style>
    </>
  )
}